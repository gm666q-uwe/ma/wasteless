import SwiftUI

@main
struct WasteLessApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
