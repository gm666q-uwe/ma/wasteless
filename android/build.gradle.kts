import space.gm666q.wasteless.build.Version
import space.gm666q.wasteless.build.dependencyNotation

android {
    buildFeatures {
        compose = true
    }
    buildToolsVersion = libs.versions.android.buildToolsVersion.get()
    buildTypes {
        debug {
            applicationIdSuffix = ".d"
        }
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    compileSdk = libs.versions.android.compileSdk.orNull?.toIntOrNull()
    composeOptions {
        kotlinCompilerExtensionVersion = libs.versions.androidx.compose.compiler.orNull
    }
    defaultConfig {
        applicationId = "space.gm666q.wasteless"
        minSdk = libs.versions.android.minSdk.orNull?.toIntOrNull()
        targetSdk = libs.versions.android.targetSdk.orNull?.toIntOrNull()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
        versionCode = Version.app.code
        versionName = Version.app.name
    }
    kotlinOptions {
        freeCompilerArgs = listOf("-Xopt-in=kotlin.RequiresOptIn")
        jvmTarget = "11"
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/AL2.0"
            excludes += "/META-INF/LGPL2.1"
        }
    }
}

dependencies {
    androidTestImplementation(libs.bundles.android.androidTest)

    coreLibraryDesugaring(libs.android.tools.desugarJDKLibs)

    debugImplementation(kotlin("reflect"))
    debugImplementation(libs.bundles.android.debug)

    implementation(project(":common"))
    implementation(libs.bundles.android.main)

    testImplementation(kotlin("test-junit"))
    testImplementation(libs.bundles.android.test)

    configurations.configureEach {
        resolutionStrategy {
            force(libs.junit.junit.get().dependencyNotation)
        }
    }
}

plugins {
    id("com.android.application")
    kotlin("android")
}
