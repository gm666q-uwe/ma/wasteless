package space.gm666q.wasteless.android.model

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.model.CategoryId
import space.gm666q.wasteless.model.CategoryId.*

enum class Category(val id: CategoryId, @DrawableRes val image: Int, @StringRes val title: Int) {
    Food(FOOD, R.drawable.food, R.string.category_food),
    Medicine(MEDICINE, R.drawable.medicine, R.string.category_medicine),
    PersonalCare(PERSONAL_CARE, R.drawable.personal_care, R.string.category_personal_care)
}
