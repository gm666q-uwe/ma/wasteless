package space.gm666q.wasteless.android.ui.component

import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ArrowBack
import androidx.compose.material.icons.twotone.Close
import androidx.compose.material.icons.twotone.DeleteForever
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.*

@Composable
fun WasteLessTopAppBar(
    onBack: () -> Unit,
    onCancel: () -> Unit,
    onDelete: () -> Unit,
    title: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    scaffoldMode: WasteLessScaffoldMode = Normal
) {
    InsetsAwareTopAppBar(title, modifier = modifier, navigationIcon =
    when (scaffoldMode) {
        Detail, DetailSave -> {
            {
                IconButton(onBack) {
                    Icon(
                        Icons.TwoTone.ArrowBack,
                        stringResource(R.string.cd_navigate_up)
                    )
                }
            }
        }
        DetailSelect, NormalSelect -> {
            {
                IconButton(onCancel) {
                    Icon(
                        Icons.TwoTone.Close,
                        stringResource(R.string.cd_cancel)
                    )
                }
            }
        }
        Normal -> null
    }, actions = {
        when (scaffoldMode) {
            Detail, DetailSave, Normal -> {
            }
            DetailSelect, NormalSelect -> {
                IconButton(onDelete) {
                    Icon(
                        Icons.TwoTone.DeleteForever,
                        stringResource(R.string.cd_delete)
                    )
                }
            }
        }
    }, backgroundColor = when (scaffoldMode) {
        Detail, DetailSave -> MaterialTheme.colors.primarySurface
        DetailSelect, NormalSelect -> MaterialTheme.colors.secondary
        Normal -> Color.Transparent
    }, elevation = when (scaffoldMode) {
        Detail, DetailSave, DetailSelect, NormalSelect -> AppBarDefaults.TopAppBarElevation
        Normal -> 0.dp
    }
    )
}
