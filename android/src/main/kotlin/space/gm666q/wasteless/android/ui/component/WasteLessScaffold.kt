package space.gm666q.wasteless.android.ui.component

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.model.toCategory
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.*
import space.gm666q.wasteless.android.ui.navigation.Screen
import space.gm666q.wasteless.android.ui.navigation.Screen.*
import space.gm666q.wasteless.android.ui.navigation.View

enum class WasteLessScaffoldMode {
    Detail,
    DetailSave,
    DetailSelect,
    Normal,
    NormalSelect,
}

@Composable
fun WasteLessScaffold(
    onBack: () -> Unit,
    onCancel: () -> Unit,
    onDelete: () -> Unit,
    onDrawer: () -> Unit,
    onNavigate: (Screen, View?) -> Unit,
    onSave: () -> Unit,
    screen: Screen,
    view: View,
    modifier: Modifier = Modifier,
    isSelect: Boolean = false,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    content: @Composable (PaddingValues) -> Unit
) {
    WasteLessScaffold(
        onBack,
        onCancel,
        onDelete,
        onDrawer,
        onNavigate,
        onSave,
        {
            when (screen) {
                is Category -> Text(stringResource(screen.categoryId.toCategory().title))
                is Edit -> Text(stringResource(if (screen.productId == null) R.string.screen_add else R.string.screen_edit))
                Home -> Text(stringResource(view.title))
                Settings -> Text(stringResource(R.string.screen_settings))
            }
        },
        view,
        modifier,
        when (screen) {
            is Category -> if (isSelect) DetailSelect else Detail
            is Edit -> Detail
            Home -> if (isSelect) NormalSelect else Normal
            Settings -> Detail
        },
        scaffoldState,
        content
    )
}

@Composable
internal fun WasteLessScaffold(
    onBack: () -> Unit,
    onCancel: () -> Unit,
    onDelete: () -> Unit,
    onDrawer: () -> Unit,
    onNavigate: (Screen, View?) -> Unit,
    onSave: () -> Unit,
    title: @Composable () -> Unit,
    view: View,
    modifier: Modifier = Modifier,
    scaffoldMode: WasteLessScaffoldMode = Normal,
    scaffoldState: ScaffoldState = rememberScaffoldState(),
    content: @Composable (PaddingValues) -> Unit
) {
    val fabShape = MaterialTheme.shapes.small.copy(CornerSize(percent = 50))

    Scaffold(
        modifier = modifier,
        scaffoldState = scaffoldState,
        topBar = {
            WasteLessTopAppBar(
                onBack,
                onCancel,
                onDelete,
                title,
                scaffoldMode = scaffoldMode
            )
        },
        bottomBar = {
            WasteLessBottomAppBar(
                onDrawer,
                cutoutShape = fabShape,
                scaffoldMode = scaffoldMode
            )
        },
        floatingActionButton = {
            WasteLessFloatingActionButton({
                when (scaffoldMode) {
                    Detail, DetailSave, DetailSelect -> onSave()
                    Normal, NormalSelect -> onNavigate(Edit(null), null)
                }
            }, scaffoldMode = scaffoldMode, shape = fabShape)
        },
        floatingActionButtonPosition = when (scaffoldMode) {
            Detail, DetailSave, DetailSelect -> FabPosition.End
            Normal, NormalSelect -> FabPosition.Center
        },
        isFloatingActionButtonDocked = when (scaffoldMode) {
            Detail, DetailSave, DetailSelect -> false
            Normal, NormalSelect -> true
        },
        drawerContent = { WasteLessDrawer(view, onNavigate) },
        drawerGesturesEnabled = when (scaffoldMode) {
            Detail, DetailSave, DetailSelect, NormalSelect -> false
            Normal -> true
        },
        content = content
    )
}
