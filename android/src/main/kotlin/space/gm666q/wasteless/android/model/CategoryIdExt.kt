package space.gm666q.wasteless.android.model

import space.gm666q.wasteless.android.model.Category.*
import space.gm666q.wasteless.model.CategoryId
import space.gm666q.wasteless.model.CategoryId.*

fun CategoryId.toCategory() = when (this) {
    FOOD -> Food
    MEDICINE -> Medicine
    PERSONAL_CARE -> PersonalCare
}
