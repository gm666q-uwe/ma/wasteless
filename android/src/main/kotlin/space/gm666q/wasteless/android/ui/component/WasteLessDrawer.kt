package space.gm666q.wasteless.android.ui.component

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.ColorFilter
import androidx.compose.ui.graphics.painter.Painter
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.graphics.vector.rememberVectorPainter
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.statusBarsHeight
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.ui.navigation.Screen
import space.gm666q.wasteless.android.ui.navigation.Screen.Home
import space.gm666q.wasteless.android.ui.navigation.Screen.Settings
import space.gm666q.wasteless.android.ui.navigation.View

@Composable
fun ColumnScope.WasteLessDrawer(
    currentView: View,
    onNavigate: (Screen, View?) -> Unit
) {
    val views = remember { View.values() }

    Spacer(Modifier.statusBarsHeight())
    Text(
        stringResource(R.string.app_name),
        modifier = Modifier.padding(16.dp),
        style = MaterialTheme.typography.h5
    )
    Divider()
    views.forEach { view ->
        DrawerButton(view.icon, view.title, currentView == view, {
            if (currentView != view) {
                onNavigate(Home, view)
            }
        })
    }
    Divider(modifier = Modifier.padding(top = 8.dp))
    DrawerButton(
        Icons.TwoTone.Settings,
        stringResource(R.string.screen_settings),
        false,
        { onNavigate(Settings, null) })
}

@Composable
private fun DrawerButton(
    @DrawableRes icon: Int,
    @StringRes label: Int,
    isSelected: Boolean,
    action: () -> Unit,
    modifier: Modifier = Modifier
) {
    DrawerButton(
        painterResource(icon),
        stringResource(label),
        isSelected,
        action,
        modifier = modifier
    )
}

@Composable
private fun DrawerButton(
    icon: ImageVector,
    label: String,
    isSelected: Boolean,
    action: () -> Unit,
    modifier: Modifier = Modifier
) {
    DrawerButton(rememberVectorPainter(icon), label, isSelected, action, modifier = modifier)
}

@Composable
private fun DrawerButton(
    icon: Painter,
    label: String,
    isSelected: Boolean,
    action: () -> Unit,
    modifier: Modifier = Modifier
) {
    val backgroundColor = if (isSelected) {
        MaterialTheme.colors.primary.copy(alpha = 0.12f)
    } else {
        Color.Transparent
    }

    val imageAlpha = if (isSelected) {
        1.0f
    } else {
        0.6f
    }

    val surfaceModifier = modifier
        .fillMaxWidth()
        .padding(end = 8.dp, start = 8.dp, top = 8.dp)

    val textIconColor = if (isSelected) {
        MaterialTheme.colors.primary
    } else {
        MaterialTheme.colors.onSurface.copy(alpha = 0.6f)
    }

    Surface(
        color = backgroundColor,
        modifier = surfaceModifier,
        shape = MaterialTheme.shapes.small
    ) {
        TextButton(
            action,
            modifier = Modifier.fillMaxWidth()
        ) {
            Row(
                horizontalArrangement = Arrangement.Start,
                modifier = Modifier.fillMaxWidth(),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Image(
                    icon,
                    null, // decorative
                    alpha = imageAlpha,
                    colorFilter = ColorFilter.tint(textIconColor)
                )
                Spacer(Modifier.width(16.dp))
                Text(
                    label,
                    color = textIconColor,
                    style = MaterialTheme.typography.body2
                )
            }
        }
    }
}
