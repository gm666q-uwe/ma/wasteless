package space.gm666q.wasteless.android

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import co.touchlab.kermit.Kermit
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.component.KoinComponent
import org.koin.core.context.startKoin
import space.gm666q.wasteless.android.di.appModule
import space.gm666q.wasteless.db.DriverFactory
import space.gm666q.wasteless.di.LoggerFactory
import space.gm666q.wasteless.di.commonModule

class WasteLessApplication : Application(), KoinComponent {
    private val logger: Kermit by inject()

    override fun onCreate() {
        super.onCreate()

        val driverFactory = DriverFactory(this@WasteLessApplication)
        val loggerFactory = LoggerFactory()

        startKoin {
            androidContext(this@WasteLessApplication)
            androidLogger()
            modules(appModule)
            modules(commonModule(driverFactory, loggerFactory))
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                NOTIFICATION_CHANNEL_EXPIRATIONS_ID,
                getString(R.string.notification_channel_expirations_name),
                NotificationManager.IMPORTANCE_DEFAULT
            ).apply {
                description = getString(R.string.notification_channel_expirations_description)
            }
            // Register the channel with the system
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }

        logger.d { "WasteLessApplication" }
    }

    companion object {
        const val NOTIFICATION_CHANNEL_EXPIRATIONS_ID = "EXPIRATIONS"
    }
}
