package space.gm666q.wasteless.android.util

import android.os.Bundle

/*fun Bundle.getLongOrThrow(key: String) =
    requireNotNull(getLong(key)) { "Missing key '$key' in $this" }*/

fun Bundle.getStringOrThrow(key: String) =
    requireNotNull(getString(key)) { "Missing key '$key' in $this" }
