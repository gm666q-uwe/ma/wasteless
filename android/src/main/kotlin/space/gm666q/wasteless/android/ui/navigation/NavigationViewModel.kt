package space.gm666q.wasteless.android.ui.navigation

import android.os.Bundle
import androidx.annotation.MainThread
import androidx.compose.runtime.getValue
import androidx.compose.runtime.setValue
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import space.gm666q.wasteless.android.ui.navigation.Screen.*
import space.gm666q.wasteless.android.ui.navigation.View.Categories
import space.gm666q.wasteless.android.ui.navigation.View.GoodList
import space.gm666q.wasteless.android.util.getMutableStateOf
import space.gm666q.wasteless.model.CategoryId

class NavigationViewModel(savedStateHandle: SavedStateHandle) : ViewModel() {
    var currentScreen: Screen by savedStateHandle.getMutableStateOf<Screen>(
        SIS_SCREEN,
        Home,
        Screen::toBundle,
        Bundle::toScreen
    )
        private set

    var currentView: View by savedStateHandle.getMutableStateOf(
        SIS_VIEW,
        GoodList,
        View::toBundle,
        Bundle::toView
    )
        private set

    private val _drawerShouldBeOpened = MutableLiveData(false)
    val drawerShouldBeOpened: LiveData<Boolean> = _drawerShouldBeOpened

    @MainThread
    fun navigateTo(screen: Screen, view: View? = null) {
        if (screen is Home) {
            currentView = view!!
        }
        currentScreen = screen
    }

    @MainThread
    fun onBack(categoryId: CategoryId? = null): Boolean {
        val wasHandled = currentScreen !is Home
        currentScreen = if (currentScreen is Edit && currentView == Categories) {
            if (categoryId == null) Home else Category(categoryId)
        } else {
            Home
        }
        return wasHandled
    }

    @MainThread
    fun openDrawer() {
        _drawerShouldBeOpened.value = true
    }

    @MainThread
    fun resetOpenDrawerAction() {
        _drawerShouldBeOpened.value = false
    }
}
