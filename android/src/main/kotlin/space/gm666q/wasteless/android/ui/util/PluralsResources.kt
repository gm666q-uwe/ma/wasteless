package space.gm666q.wasteless.android.ui.util

import android.content.res.Resources
import androidx.annotation.PluralsRes
import androidx.compose.runtime.Composable
import androidx.compose.runtime.ReadOnlyComposable
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.platform.LocalContext

@Composable
@ReadOnlyComposable
fun pluralsResource(@PluralsRes id: Int, quantity: Int): String {
    val resources = resources()
    return resources.getQuantityString(id, quantity)
}

@Composable
@ReadOnlyComposable
fun pluralsResource(@PluralsRes id: Int, quantity: Int, vararg formatArgs: Any): String {
    val resources = resources()
    return resources.getQuantityString(id, quantity, *formatArgs)
}

@Composable
@ReadOnlyComposable
private fun resources(): Resources {
    LocalConfiguration.current
    return LocalContext.current.resources
}
