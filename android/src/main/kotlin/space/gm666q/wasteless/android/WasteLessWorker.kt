package space.gm666q.wasteless.android

import android.content.Context
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.Worker
import androidx.work.WorkerParameters
import co.touchlab.kermit.Kermit
import kotlinx.datetime.*
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import space.gm666q.wasteless.repository.WasteLessRepository

class WasteLessWorker(context: Context, workerParams: WorkerParameters) :
    Worker(context, workerParams), KoinComponent {
    private val logger: Kermit by inject()
    private val wasteLessRepository: WasteLessRepository by inject()

    override fun doWork(): Result {
        val systemTimeZone = TimeZone.currentSystemDefault()
        val tomorrow =
            Clock.System.now().toLocalDateTime(systemTimeZone).date.plus(1, DateTimeUnit.DAY)

        val productCount = wasteLessRepository.countProducts(tomorrow)
        if (productCount > 0) {
            val builder =
                NotificationCompat.Builder(
                    applicationContext,
                    WasteLessApplication.NOTIFICATION_CHANNEL_EXPIRATIONS_ID
                )
                    .setContentTitle(applicationContext.resources.getString(R.string.notification_expirations_title))
                    .setContentText(
                        applicationContext.resources.getQuantityString(
                            R.plurals.notification_expirations_text,
                            productCount.toInt(),
                            productCount
                        )
                    )
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setSmallIcon(R.drawable.ic_stat_expirations)
            with(NotificationManagerCompat.from(applicationContext)) {
                notify(
                    tomorrow.atStartOfDayIn(systemTimeZone).epochSeconds.toInt(),
                    builder.build()
                )
            }
        }

        return Result.success()
    }

    companion object {
        const val WORK_NAME = "WASTE_LESS_WORK"
    }
}
