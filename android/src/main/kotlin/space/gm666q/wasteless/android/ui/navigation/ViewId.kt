package space.gm666q.wasteless.android.ui.navigation

enum class ViewId {
    GOOD_LIST,
    EXPIRED_LIST,
    CATEGORIES,
}
