package space.gm666q.wasteless.android.ui

import android.content.Intent
import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.animation.Crossfade
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.ScaffoldState
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.core.view.WindowCompat
import com.google.accompanist.insets.ProvideWindowInsets
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import com.google.android.material.composethemeadapter.MdcTheme
import kotlinx.coroutines.launch
import org.koin.androidx.compose.get
import space.gm666q.wasteless.android.model.toCategory
import space.gm666q.wasteless.android.ui.component.WasteLessScaffold
import space.gm666q.wasteless.android.ui.edit.EditScreen
import space.gm666q.wasteless.android.ui.home.CategoriesScreen
import space.gm666q.wasteless.android.ui.home.CategoryListScreen
import space.gm666q.wasteless.android.ui.home.ExpiredListScreen
import space.gm666q.wasteless.android.ui.home.GoodListScreen
import space.gm666q.wasteless.android.ui.navigation.*
import space.gm666q.wasteless.android.ui.navigation.Screen.*
import space.gm666q.wasteless.android.ui.navigation.View.*
import space.gm666q.wasteless.android.ui.settings.SettingsScreen
import space.gm666q.wasteless.android.ui.util.BackPressHandler
import space.gm666q.wasteless.android.ui.util.LocalBackPressedDispatcher
import space.gm666q.wasteless.model.CategoryId
import space.gm666q.wasteless.model.Product
import space.gm666q.wasteless.repository.WasteLessRepository

class WasteLessActivity : AppCompatActivity() {
    private val navigationViewModel: NavigationViewModel by viewModels()

    private fun handleIntent(intent: Intent?) {
        intent?.getStringExtra("view")?.let {
            navigationViewModel.navigateTo(Home, ViewId.valueOf(it).toView())
        }
    }

    override fun onBackPressed() {
        if (!navigationViewModel.onBack()) {
            super.onBackPressed()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        handleIntent(intent)

        WindowCompat.setDecorFitsSystemWindows(window, false)

        setContent {
            ProvideWindowInsets(consumeWindowInsets = false) {
                CompositionLocalProvider(LocalBackPressedDispatcher provides this.onBackPressedDispatcher) {
                    val scaffoldState = rememberScaffoldState()

                    val openDrawerEvent = navigationViewModel.drawerShouldBeOpened.observeAsState()
                    if (openDrawerEvent.value == true) {
                        LaunchedEffect(Unit) {
                            scaffoldState.drawerState.open()
                            navigationViewModel.resetOpenDrawerAction()
                        }
                    }

                    val coroutineScope = rememberCoroutineScope()
                    if (scaffoldState.drawerState.isOpen) {
                        BackPressHandler {
                            coroutineScope.launch {
                                scaffoldState.drawerState.close()
                            }
                        }
                    }

                    MdcTheme {
                        val darkIcons = MaterialTheme.colors.isLight
                        val systemUiController = rememberSystemUiController()

                        SideEffect {
                            systemUiController.setSystemBarsColor(
                                Color.Transparent,
                                darkIcons = darkIcons
                            )
                        }

                        WasteLessScreen(
                            navigationViewModel,
                            { navigationViewModel.onBack(it) },
                            { navigationViewModel.openDrawer() },
                            { s, v ->
                                if (scaffoldState.drawerState.isOpen) {
                                    coroutineScope.launch { scaffoldState.drawerState.close() }
                                }
                                navigationViewModel.navigateTo(s, v)
                            },
                            scaffoldState
                        )
                    }
                }
            }
        }
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        handleIntent(intent)
    }
}

@Composable
private fun WasteLessScreen(
    navigationViewModel: NavigationViewModel,
    onBack: (CategoryId?) -> Unit,
    onDrawer: () -> Unit,
    onNavigate: (Screen, View?) -> Unit,
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    val wasteLessRepository: WasteLessRepository = get()

    val selected = remember { mutableStateListOf<Product>() }

    val screen = navigationViewModel.currentScreen

    WasteLessContent(
        navigationViewModel,
        if (screen is Edit) {
            if (screen.productId == null) null else wasteLessRepository.fetchProduct(screen.productId)?.category
        } else {
            null
        },
        onBack,
        {
            selected.clear()
        },
        {
            selected.forEach { product ->
                wasteLessRepository.deleteProduct(product)
            }
            selected.clear()
        },
        {
            selected.clear()
            onDrawer()
        },
        { s, v ->
            selected.clear()
            onNavigate(s, v)
        },
        selected.toList(),
        { p ->
            val product = selected.find { it.id == p.id }
            if (product == null)
                selected.add(p)
            else
                selected.remove(product)
        },
        scaffoldState
    )
}

@Composable
private fun WasteLessContent(
    navigationViewModel: NavigationViewModel,
    category: CategoryId?,
    onBack: (CategoryId?) -> Unit,
    onCancel: () -> Unit,
    onDelete: () -> Unit,
    onDrawer: () -> Unit,
    onNavigate: (Screen, View?) -> Unit,
    selected: List<Product>,
    onSelect: (Product) -> Unit,
    scaffoldState: ScaffoldState = rememberScaffoldState()
) {
    Crossfade(navigationViewModel.currentScreen) { screen ->
        WasteLessScaffold(
            { onBack(category) },
            onCancel,
            onDelete,
            onDrawer,
            onNavigate,
            {},
            screen,
            navigationViewModel.currentView,
            isSelect = selected.isNotEmpty(),
            scaffoldState = scaffoldState
        ) { innerPadding ->
            val modifier = Modifier.padding(innerPadding)
            when (screen) {
                is Category -> CategoryListScreen(
                    screen.categoryId.toCategory(),
                    selected,
                    { product -> onNavigate(Edit(product.id), null) },
                    onSelect,
                    modifier
                )
                is Edit -> EditScreen(screen.productId, onBack, modifier)
                Home -> Crossfade(navigationViewModel.currentView) { view ->
                    when (view) {
                        GoodList -> GoodListScreen(
                            selected,
                            { product -> onNavigate(Edit(product.id), null) },
                            onSelect,
                            modifier
                        )
                        ExpiredList -> ExpiredListScreen(
                            selected,
                            { product -> onNavigate(Edit(product.id), null) },
                            onSelect,
                            modifier
                        )
                        Categories -> CategoriesScreen(
                            { onNavigate(Category(it.id), null) },
                            modifier
                        )
                    }
                }
                Settings -> SettingsScreen(modifier)
            }
        }
    }
}
