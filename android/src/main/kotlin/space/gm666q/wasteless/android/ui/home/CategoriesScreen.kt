package space.gm666q.wasteless.android.ui.home

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import org.koin.androidx.compose.getViewModel
import space.gm666q.wasteless.android.model.Category
import space.gm666q.wasteless.android.ui.WasteLessViewModel

@Composable
fun CategoriesScreen(onCategory: (Category) -> Unit, modifier: Modifier = Modifier) {
    val wasteLessViewModel: WasteLessViewModel = getViewModel()

    val categories = remember { Category.values() }

    LazyColumn(
        modifier = Modifier.fillMaxWidth(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        items(categories) { category ->
            val products = wasteLessViewModel.productsCategory(category).collectAsState()
            CategoryCard(
                category,
                products.value.size,
                modifier = Modifier
                    .clickable { onCategory(category) }
                    .fillMaxWidth()
                    .padding(horizontal = 8.dp, vertical = 4.dp)
            )
        }
        item {
            Spacer(modifier = modifier)
        }
    }
}
