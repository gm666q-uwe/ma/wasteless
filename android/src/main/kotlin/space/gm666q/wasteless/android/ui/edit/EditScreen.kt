package space.gm666q.wasteless.android.ui.edit

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.Icon
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusDirection
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalFocusManager
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.unit.dp
import kotlinx.datetime.LocalDate
import org.koin.androidx.compose.get
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.model.toCategory
import space.gm666q.wasteless.android.ui.component.WasteLessFloatingActionButton
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.DetailSave
import space.gm666q.wasteless.model.CategoryId
import space.gm666q.wasteless.model.Product
import space.gm666q.wasteless.repository.WasteLessRepository

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun EditScreen(productId: Long?, onBack: (CategoryId?) -> Unit, modifier: Modifier = Modifier) {
    val wasteLessRepository: WasteLessRepository = get()
    val product = if (productId == null) null else wasteLessRepository.fetchProduct(productId)

    EditScreen(product, {
        if (productId == null) {
            wasteLessRepository.insertProduct(it)
        } else {
            wasteLessRepository.updateProduct(it)
        }
        onBack(it.category)
    }, modifier)
}

@Composable
fun EditScreen(
    product: Product?,
    onSave: (Product) -> Unit,
    modifier: Modifier = Modifier
) {
    var category: CategoryId? by remember { mutableStateOf(product?.category) }
    var categoryIsError by remember { mutableStateOf(false) }
    var expirationDate: LocalDate? by remember { mutableStateOf(product?.expiration_date) }
    var expirationDateIsError by remember { mutableStateOf(false) }
    var name by remember { mutableStateOf(product?.name ?: "") }
    var nameIsError by remember { mutableStateOf(false) }
    var quantity by remember { mutableStateOf(product?.quantity?.toString() ?: "") }
    var quantityIsError by remember { mutableStateOf(false) }

    EditContent(
        category,
        {
            category = it
            categoryIsError = categoryIsError(category)
        },
        expirationDate,
        {
            expirationDate = it
            expirationDateIsError = expirationDateIsError(expirationDate)
        },
        name,
        {
            name = it
            nameIsError = nameIsError(name)
        },
        quantity,
        {
            quantity = it
            quantityIsError = quantityIsError(quantity)
        },
        {
            categoryIsError = categoryIsError(category)
            expirationDateIsError = expirationDateIsError(expirationDate)
            nameIsError = nameIsError(name)
            quantityIsError = quantityIsError(quantity)
            if (!categoryIsError && !expirationDateIsError && !nameIsError && !quantityIsError) {
                onSave(Product(product?.id, category!!, expirationDate!!, name, quantity.toLong()))
            }
        },
        modifier,
        categoryIsError,
        expirationDateIsError,
        nameIsError,
        quantityIsError
    )
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun EditContent(
    category: CategoryId?,
    onCategoryChange: (CategoryId?) -> Unit,
    expirationDate: LocalDate?,
    onExpirationDateChange: (LocalDate?) -> Unit,
    name: String,
    onNameChange: (String) -> Unit,
    quantity: String,
    onQuantityChange: (String) -> Unit,
    onSave: () -> Unit,
    modifier: Modifier = Modifier,
    categoryIsError: Boolean = false,
    expirationDateIsError: Boolean = false,
    nameIsError: Boolean = false,
    quantityIsError: Boolean = false
) {
    val focusManager = LocalFocusManager.current
    val focusRequester = remember { FocusRequester() }

    LaunchedEffect(Unit) {
        focusRequester.requestFocus()
    }

    Scaffold(floatingActionButton = {
        WasteLessFloatingActionButton(onSave, scaffoldMode = DetailSave)
    }) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            modifier = modifier.fillMaxWidth()
        ) {
            FieldWithCaption(
                modifier = Modifier.padding(vertical = 4.dp),
                errorResource = R.string.field_name_error,
                isError = nameIsError
            ) {
                OutlinedTextField(
                    name,
                    onNameChange,
                    modifier = Modifier
                        .focusRequester(focusRequester),
                    label = { Text(stringResource(R.string.field_name_label)) },
                    placeholder = { Text(stringResource(R.string.field_name_placeholder)) },
                    isError = it,
                    keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                    keyboardActions = KeyboardActions(onNext = {
                        focusManager.moveFocus(
                            FocusDirection.Down
                        )
                    }),
                    singleLine = true
                )

            }
            FieldWithCaption(
                modifier = Modifier.padding(vertical = 4.dp),
                errorResource = R.string.field_quantity_error,
                isError = quantityIsError
            ) {
                OutlinedTextField(
                    quantity,
                    onQuantityChange,
                    label = { Text(stringResource(R.string.field_quantity_label)) },
                    placeholder = { Text(stringResource(R.string.field_quantity_placeholder)) },
                    isError = it,
                    keyboardOptions = KeyboardOptions(
                        keyboardType = KeyboardType.Number,
                        imeAction = ImeAction.Done
                    ),
                    keyboardActions = KeyboardActions(onDone = { focusManager.clearFocus() }),
                    singleLine = true
                )
            }
            FieldWithCaption(
                modifier = Modifier.padding(vertical = 4.dp),
                errorResource = R.string.field_category_error,
                isError = categoryIsError
            ) {
                OutlinedEnumField(
                    category,
                    onCategoryChange,
                    nameMapping = { stringResource(it.toCategory().title) },
                    label = { Text(stringResource(R.string.field_category_label)) },
                    placeholder = { Text(stringResource(R.string.field_category_placeholder)) },
                    isError = it
                )
            }
            FieldWithCaption(
                modifier = Modifier.padding(vertical = 4.dp),
                errorResource = R.string.field_expiration_date_error,
                isError = expirationDateIsError
            ) {
                OutlinedDateField(
                    expirationDate,
                    onExpirationDateChange,
                    modifier = Modifier.padding(vertical = 4.dp),
                    title = R.string.field_expiration_date_title,
                    label = { Text(stringResource(R.string.field_expiration_date_label)) },
                    placeholder = { Text(stringResource(R.string.field_expiration_date_placeholder)) },
                    trailingIcon = { Icon(Icons.TwoTone.ExpandMore, null) },
                    isError = it
                )
            }
        }
    }
}

private fun categoryIsError(value: CategoryId?) = value == null

private fun expirationDateIsError(value: LocalDate?) = value == null

private fun nameIsError(value: String) = value.isBlank()

private fun quantityIsError(value: String): Boolean {
    val l = value.toLongOrNull()
    return if (l == null) true else l < 1
}
