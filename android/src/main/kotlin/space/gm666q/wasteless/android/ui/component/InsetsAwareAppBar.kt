package space.gm666q.wasteless.android.ui.component

import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.RowScope
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.LayoutDirection
import androidx.compose.ui.unit.dp
import com.google.accompanist.insets.LocalWindowInsets
import com.google.accompanist.insets.navigationBarsPadding
import com.google.accompanist.insets.rememberInsetsPaddingValues
import com.google.accompanist.insets.statusBarsPadding

@Composable
fun InsetsAwareTopAppBar(
    title: @Composable () -> Unit,
    modifier: Modifier = Modifier,
    navigationIcon: @Composable (() -> Unit)? = null,
    actions: @Composable RowScope.() -> Unit = {},
    backgroundColor: Color = MaterialTheme.colors.primarySurface,
    contentColor: Color = contentColorFor(backgroundColor),
    elevation: Dp = AppBarDefaults.TopAppBarElevation
) {
    Surface(color = backgroundColor, elevation = elevation, modifier = modifier) {
        TopAppBar(
            title = title,
            modifier = Modifier
                .navigationBarsPadding(bottom = false)
                .statusBarsPadding(),
            navigationIcon = navigationIcon,
            actions = actions,
            backgroundColor = Color.Transparent,
            contentColor = contentColor,
            elevation = 0.dp
        )
    }
}

@Composable
fun InsetsAwareTopAppBar(
    modifier: Modifier = Modifier,
    backgroundColor: Color = MaterialTheme.colors.primarySurface,
    contentColor: Color = contentColorFor(backgroundColor),
    elevation: Dp = AppBarDefaults.TopAppBarElevation,
    contentPadding: PaddingValues = AppBarDefaults.ContentPadding,
    content: @Composable RowScope.() -> Unit
) {
    Surface(color = backgroundColor, elevation = elevation, modifier = modifier) {
        TopAppBar(
            modifier = Modifier
                .navigationBarsPadding(bottom = false)
                .statusBarsPadding(),
            backgroundColor = Color.Transparent,
            contentColor = contentColor,
            elevation = 0.dp,
            contentPadding = contentPadding,
            content
        )
    }
}

@Composable
fun InsetsAwareBottomAppBar(
    modifier: Modifier = Modifier,
    backgroundColor: Color = MaterialTheme.colors.primarySurface,
    contentColor: Color = contentColorFor(backgroundColor),
    cutoutShape: Shape? = null,
    elevation: Dp = AppBarDefaults.BottomAppBarElevation,
    contentPadding: PaddingValues = AppBarDefaults.ContentPadding,
    content: @Composable RowScope.() -> Unit
) {
    val layoutDirection = LocalLayoutDirection.current
    BottomAppBar(
        modifier = modifier,
        backgroundColor = backgroundColor,
        contentColor = contentColor,
        cutoutShape = cutoutShape,
        elevation = elevation,
        contentPadding = rememberInsetsPaddingValues(
            LocalWindowInsets.current.navigationBars,
            additionalStart = if (layoutDirection == LayoutDirection.Ltr) contentPadding.calculateLeftPadding(
                layoutDirection
            ) else contentPadding.calculateRightPadding(layoutDirection),
            additionalTop = contentPadding.calculateTopPadding(),
            additionalEnd = if (layoutDirection == LayoutDirection.Ltr) contentPadding.calculateRightPadding(
                layoutDirection
            ) else contentPadding.calculateLeftPadding(layoutDirection),
            additionalBottom = contentPadding.calculateBottomPadding()
        ),
        content
    )
}
