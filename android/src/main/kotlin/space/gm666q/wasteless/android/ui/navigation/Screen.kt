package space.gm666q.wasteless.android.ui.navigation

import space.gm666q.wasteless.android.ui.navigation.ScreenId.*
import space.gm666q.wasteless.model.CategoryId

sealed class Screen(val id: ScreenId) {
    data class Category(val categoryId: CategoryId) : Screen(CATEGORY)
    data class Edit(val productId: Long?) : Screen(EDIT)
    object Home : Screen(HOME)
    object Settings : Screen(SETTINGS)
}
