package space.gm666q.wasteless.android.ui.navigation

import android.os.Bundle
import androidx.core.os.bundleOf
import space.gm666q.wasteless.android.ui.navigation.Screen.*
import space.gm666q.wasteless.android.ui.navigation.ScreenId.*
import space.gm666q.wasteless.android.util.getStringOrThrow
import space.gm666q.wasteless.model.CategoryId

private const val SIS_CATEGORY = "category"
private const val SIS_ID = "screen_id"
internal const val SIS_SCREEN = "sis_screen"
private const val SIS_PRODUCT = "product"

internal fun Bundle.toScreen() = when (ScreenId.valueOf(getStringOrThrow(SIS_ID))) {
    CATEGORY -> {
        Category(CategoryId.valueOf(getStringOrThrow(SIS_CATEGORY)))
    }
    EDIT -> {
        val product = getLong(SIS_PRODUCT)
        Edit(if (product == -1L) null else product)
    }
    HOME -> Home
    SETTINGS -> Settings
}

internal fun Screen.toBundle() = bundleOf(SIS_ID to id.name).also {
    when (this) {
        is Category -> it.putString(SIS_CATEGORY, categoryId.name)
        is Edit -> it.putLong(SIS_PRODUCT, productId ?: -1L)
        else -> {
        }
    }
}
