package space.gm666q.wasteless.android.ui.theme

import androidx.compose.material.Typography
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import space.gm666q.wasteless.android.R

private val FiraSans = FontFamily(
    Font(R.font.firasans_black, weight = FontWeight.Black, style = FontStyle.Normal),
    Font(R.font.firasans_blackitalic, weight = FontWeight.Black, style = FontStyle.Italic),
    Font(R.font.firasans_bold, weight = FontWeight.Bold, style = FontStyle.Normal),
    Font(R.font.firasans_bolditalic, weight = FontWeight.Bold, style = FontStyle.Italic),
    Font(R.font.firasans_extrabold, weight = FontWeight.ExtraBold, style = FontStyle.Normal),
    Font(R.font.firasans_extrabolditalic, weight = FontWeight.ExtraBold, style = FontStyle.Italic),
    Font(R.font.firasans_extralight, weight = FontWeight.ExtraLight, style = FontStyle.Normal),
    Font(
        R.font.firasans_extralightitalic,
        weight = FontWeight.ExtraLight,
        style = FontStyle.Italic
    ),
    Font(R.font.firasans_italic, weight = FontWeight.Normal, style = FontStyle.Italic),
    Font(R.font.firasans_light, weight = FontWeight.Light, style = FontStyle.Normal),
    Font(R.font.firasans_lightitalic, weight = FontWeight.Light, style = FontStyle.Italic),
    Font(R.font.firasans_medium, weight = FontWeight.Medium, style = FontStyle.Normal),
    Font(R.font.firasans_mediumitalic, weight = FontWeight.Medium, style = FontStyle.Italic),
    Font(R.font.firasans_regular, weight = FontWeight.Normal, style = FontStyle.Normal),
    Font(R.font.firasans_semibold, weight = FontWeight.SemiBold, style = FontStyle.Normal),
    Font(R.font.firasans_semibolditalic, weight = FontWeight.SemiBold, style = FontStyle.Italic),
    Font(R.font.firasans_thin, weight = FontWeight.Thin, style = FontStyle.Normal),
    Font(R.font.firasans_thinitalic, weight = FontWeight.Thin, style = FontStyle.Italic)
)

val typography = Typography(defaultFontFamily = FiraSans)
