package space.gm666q.wasteless.android.ui.edit

import androidx.annotation.StringRes
import androidx.compose.animation.*
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource

@ExperimentalAnimationApi
@Composable
fun FieldWithCaption(
    modifier: Modifier = Modifier,
    @StringRes helpResource: Int? = null,
    @StringRes errorResource: Int? = null,
    isError: Boolean = false,
    field: @Composable (Boolean) -> Unit
) {
    FieldWithCaption(
        modifier = modifier,
        helpText = if (helpResource == null) null else {
            {
                Text(stringResource(helpResource), style = MaterialTheme.typography.caption)
            }
        },
        errorText = if (errorResource == null) null else {
            {
                Text(
                    stringResource(errorResource),
                    color = MaterialTheme.colors.error,
                    style = MaterialTheme.typography.caption
                )
            }
        },
        isError = isError,
        field = field
    )
}

@ExperimentalAnimationApi
@Composable
fun FieldWithCaption(
    modifier: Modifier = Modifier,
    helpText: @Composable (() -> Unit)? = null,
    errorText: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    field: @Composable (Boolean) -> Unit
) {
    Column(modifier = modifier) {
        field(isError)
        Box {
            if (helpText != null) {
                this@Column.AnimatedVisibility(
                    if (errorText == null) true else !isError,
                    enter = fadeIn() + slideInVertically(),
                    exit = fadeOut() + slideOutVertically()
                ) {
                    helpText()
                }
            }
            if (errorText != null) {
                this@Column.AnimatedVisibility(
                    isError,
                    enter = fadeIn() + slideInVertically(),
                    exit = fadeOut() + slideOutVertically()
                ) {
                    errorText()
                }
            }
        }
    }
}
