package space.gm666q.wasteless.android.ui.navigation

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.ui.navigation.ViewId.*

enum class View(val id: ViewId, @DrawableRes val icon: Int, @StringRes val title: Int) {
    GoodList(GOOD_LIST, R.drawable.ic_good_list, R.string.view_good_list),
    ExpiredList(EXPIRED_LIST, R.drawable.ic_expired_list, R.string.view_expired_list),
    Categories(CATEGORIES, R.drawable.ic_categories, R.string.view_categories)
}
