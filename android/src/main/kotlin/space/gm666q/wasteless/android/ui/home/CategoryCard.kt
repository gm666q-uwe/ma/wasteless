package space.gm666q.wasteless.android.ui.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Card
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.model.Category

@Composable
fun CategoryCard(
    category: Category,
    products: Int,
    modifier: Modifier = Modifier
) {
    Card(
        modifier = modifier.aspectRatio(7.0f / 5.0f)/*.size(280.dp, 260.dp)*/,
        shape = MaterialTheme.shapes.medium,
        elevation = 4.dp
    ) {
        Column {
            Image(
                contentDescription = null, // decorative
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .fillMaxWidth()
                    .fillMaxHeight(0.72f),
                painter = painterResource(category.image)
            )
            Column(modifier = Modifier.padding(16.dp)) {
                Text(
                    stringResource(category.title),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    style = MaterialTheme.typography.h6,
                )
                Text(
                    stringResource(R.string.card_category_products, products),
                    overflow = TextOverflow.Ellipsis,
                    maxLines = 1,
                    style = MaterialTheme.typography.caption
                )
            }
        }
    }
}
