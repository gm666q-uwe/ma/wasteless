package space.gm666q.wasteless.android.ui.settings

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.selection.toggleable
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.booleanResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.semantics.semantics
import androidx.compose.ui.unit.dp
import androidx.work.*
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.WasteLessWorker
import java.time.Duration

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun SettingsScreen(modifier: Modifier = Modifier) {
    val context = LocalContext.current
    val workInfos = WorkManager.getInstance(context)
        .getWorkInfosForUniqueWorkLiveData(WasteLessWorker.WORK_NAME).observeAsState()

    val notificationsEnabled = (workInfos.value?.lastOrNull()?.state
        ?: WorkInfo.State.CANCELLED) != WorkInfo.State.CANCELLED

    Column(modifier = modifier, horizontalAlignment = Alignment.CenterHorizontally) {
        ListItem(modifier
            .semantics(true) {}
            .toggleable(notificationsEnabled) {
                val workManager = WorkManager.getInstance(context)
                if (notificationsEnabled) {
                    workManager.cancelUniqueWork(WasteLessWorker.WORK_NAME)
                } else {
                    workManager.enqueueUniquePeriodicWork(
                        WasteLessWorker.WORK_NAME, ExistingPeriodicWorkPolicy.KEEP,
                        PeriodicWorkRequestBuilder<WasteLessWorker>(Duration.ofHours(12)).build()
                    )
                }
            },
            secondaryText = { Text(stringResource(R.string.settings_enable_notifications_description)) },
            trailing = {
                Checkbox(
                    notificationsEnabled,
                    {},
                    enabled = false,
                    colors = CheckboxDefaults.colors(disabledColor = MaterialTheme.colors.secondary)
                )
            }) {
            Text(stringResource(R.string.settings_enable_notifications_label))
        }
        if (booleanResource(R.bool.is_debug)) {
            Button({
                WorkManager.getInstance(context)
                    .enqueue(OneTimeWorkRequestBuilder<WasteLessWorker>().build())
            }, modifier = Modifier.padding(top = 10.dp)) {
                Text("Send test notification")
            }
        }
    }
}
