package space.gm666q.wasteless.android.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import co.touchlab.kermit.Kermit
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.datetime.LocalDate
import space.gm666q.wasteless.android.model.Category
import space.gm666q.wasteless.repository.WasteLessRepository

class WasteLessViewModel(
    private val logger: Kermit,
    private val wasteLessRepository: WasteLessRepository
) : ViewModel() {
    val products = wasteLessRepository.fetchProductsAsFlow()
        .stateIn(viewModelScope, SharingStarted.Eagerly, emptyList())

    fun product(id: Long) = wasteLessRepository.fetchProductAsFlow(id)
        .stateIn(viewModelScope, SharingStarted.Eagerly, null)

    fun productCount(date: LocalDate, less: Boolean = false) =
        wasteLessRepository.countProductsAsFlow(date, less)
            .stateIn(viewModelScope, SharingStarted.Eagerly, 0)

    fun productsCategory(category: Category, descending: Boolean = false) =
        wasteLessRepository.fetchProductsCategoryAsFlow(category.id, descending).stateIn(
            viewModelScope,
            SharingStarted.Eagerly, emptyList()
        )

    fun productsRange(low: LocalDate, high: LocalDate, descending: Boolean = false) =
        wasteLessRepository.fetchProductsRangeAsFlow(low, high, descending).stateIn(
            viewModelScope,
            SharingStarted.Eagerly, emptyList()
        )
}
