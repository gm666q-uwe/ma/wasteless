package space.gm666q.wasteless.android.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import space.gm666q.wasteless.android.ui.WasteLessViewModel

val appModule = module {
    viewModel { WasteLessViewModel(get(), get()) }
}
