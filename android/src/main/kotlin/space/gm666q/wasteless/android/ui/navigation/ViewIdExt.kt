package space.gm666q.wasteless.android.ui.navigation

import space.gm666q.wasteless.android.ui.navigation.ViewId.*
import space.gm666q.wasteless.android.ui.navigation.View.*

fun ViewId.toView() = when (this) {
    CATEGORIES -> Categories
    GOOD_LIST -> GoodList
    EXPIRED_LIST -> ExpiredList
}
