package space.gm666q.wasteless.android.ui.component

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Menu
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.stringResource
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.*

@Composable
fun WasteLessBottomAppBar(
    onDrawer: () -> Unit,
    modifier: Modifier = Modifier,
    cutoutShape: Shape = MaterialTheme.shapes.small.copy(CornerSize(percent = 50)),
    scaffoldMode: WasteLessScaffoldMode = Normal
) {
    when (scaffoldMode) {
        Detail, DetailSave, DetailSelect -> {
        }
        Normal, NormalSelect -> {
            InsetsAwareBottomAppBar(modifier = modifier, cutoutShape = cutoutShape) {
                IconButton(onDrawer) {
                    Icon(Icons.TwoTone.Menu, stringResource(R.string.cd_open_drawer))
                }
                Spacer(
                    Modifier
                        .fillMaxWidth()
                        .weight(1.0f)
                )
                /*IconButton({}) {
                    Icon(Icons.TwoTone.MoreVert, null)
                }*/
            }
        }
    }
}
