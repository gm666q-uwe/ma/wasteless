package space.gm666q.wasteless.android.ui.navigation

import android.os.Bundle
import androidx.core.os.bundleOf
import space.gm666q.wasteless.android.util.getStringOrThrow

private const val SIS_ID = "view_id"
internal const val SIS_VIEW = "sis_view"

internal fun Bundle.toView() = ViewId.valueOf(getStringOrThrow(SIS_ID)).toView()

internal fun View.toBundle() = bundleOf(SIS_ID to id.name)
