package space.gm666q.wasteless.android.ui.edit

import androidx.annotation.StringRes
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation
import androidx.fragment.app.FragmentActivity
import com.google.android.material.datepicker.MaterialDatePicker
import kotlinx.datetime.*
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun DateField(
    value: LocalDate?,
    onValueChange: (LocalDate?) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    @StringRes title: Int = 0,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    //keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    //keyboardActions: KeyboardActions = KeyboardActions(),
    //singleLine: Boolean = false,
    //maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape =
        MaterialTheme.shapes.small.copy(bottomEnd = ZeroCornerSize, bottomStart = ZeroCornerSize),
    colors: TextFieldColors = TextFieldDefaults.textFieldColors()
) {
    val activity = LocalContext.current as FragmentActivity
    val isFocused = interactionSource.collectIsFocusedAsState().value
    val isPressed = interactionSource.collectIsPressedAsState().value

    val formatter = remember { DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT) }
    val systemTimeZone = remember { TimeZone.currentSystemDefault() }
    var isOpen by remember { mutableStateOf(false) }
    var wasFocused by remember { mutableStateOf(isFocused) }
    var textFieldValueState by remember {
        mutableStateOf(
            TextFieldValue(
                text = value?.toJavaLocalDate()?.format(formatter) ?: ""
            )
        )
    }
    val textFieldValue =
        textFieldValueState.copy(text = value?.toJavaLocalDate()?.format(formatter) ?: "")

    val isFocusedNow = if (wasFocused) false else isFocused

    if (!isOpen && (isFocusedNow || isPressed) && !readOnly) {
        val picker =
            (if (value == null) MaterialDatePicker.Builder.datePicker() else MaterialDatePicker.Builder.datePicker()
                .setSelection(
                    value.plus(1, DateTimeUnit.DAY).atStartOfDayIn(systemTimeZone)
                        .toEpochMilliseconds()
                )).setTitleText(title).build()
        activity.let {
            picker.show(it.supportFragmentManager, picker.toString())
            picker.addOnCancelListener { isOpen = false }
            picker.addOnDismissListener { isOpen = false }
            picker.addOnNegativeButtonClickListener { isOpen = false }
            picker.addOnPositiveButtonClickListener {
                val date = Instant.fromEpochMilliseconds(it).toLocalDateTime(systemTimeZone).date
                textFieldValueState = TextFieldValue(date.toJavaLocalDate().format(formatter))
                if (value != date) {
                    onValueChange(date)
                }
                isOpen = false
            }
        }
        @Suppress("UNUSED_VALUE")
        isOpen = true
    }

    TextField(
        enabled = enabled,
        readOnly = true,
        value = textFieldValue,
        onValueChange = { },
        modifier = modifier,
        singleLine = true,
        textStyle = textStyle,
        label = label,
        placeholder = placeholder,
        leadingIcon = leadingIcon,
        trailingIcon = trailingIcon,
        isError = isError,
        visualTransformation = visualTransformation,
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.None),
        keyboardActions = KeyboardActions.Default,
        maxLines = Int.MAX_VALUE,
        interactionSource = interactionSource,
        shape = shape,
        colors = colors
    )

    @Suppress("UNUSED_VALUE")
    wasFocused = isFocused
}

@Composable
fun OutlinedDateField(
    value: LocalDate?,
    onValueChange: (LocalDate?) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    @StringRes title: Int = 0,
    label: @Composable (() -> Unit)? = null,
    placeholder: @Composable (() -> Unit)? = null,
    leadingIcon: @Composable (() -> Unit)? = null,
    trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    //keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    //keyboardActions: KeyboardActions = KeyboardActions.Default,
    //singleLine: Boolean = false,
    //maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors()
) {
    val activity = LocalContext.current as FragmentActivity
    val isFocused = interactionSource.collectIsFocusedAsState().value
    val isPressed = interactionSource.collectIsPressedAsState().value

    val formatter = remember { DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT) }
    val systemTimeZone = remember { TimeZone.currentSystemDefault() }
    var isOpen by remember { mutableStateOf(false) }
    var wasFocused by remember { mutableStateOf(isFocused) }
    var textFieldValueState by remember {
        mutableStateOf(
            TextFieldValue(
                text = value?.toJavaLocalDate()?.format(formatter) ?: ""
            )
        )
    }
    val textFieldValue =
        textFieldValueState.copy(text = value?.toJavaLocalDate()?.format(formatter) ?: "")

    val isFocusedNow = if (wasFocused) false else isFocused

    if (!isOpen && (isFocusedNow || isPressed) && !readOnly) {
        val picker =
            (if (value == null) MaterialDatePicker.Builder.datePicker() else MaterialDatePicker.Builder.datePicker()
                .setSelection(
                    value.plus(1, DateTimeUnit.DAY).atStartOfDayIn(systemTimeZone)
                        .toEpochMilliseconds()
                )).setTitleText(title).build()
        activity.let {
            picker.show(it.supportFragmentManager, picker.toString())
            picker.addOnCancelListener { isOpen = false }
            picker.addOnDismissListener { isOpen = false }
            picker.addOnNegativeButtonClickListener { isOpen = false }
            picker.addOnPositiveButtonClickListener {
                val date = Instant.fromEpochMilliseconds(it).toLocalDateTime(systemTimeZone).date
                textFieldValueState = TextFieldValue(date.toJavaLocalDate().format(formatter))
                if (value != date) {
                    onValueChange(date)
                }
                isOpen = false
            }
        }
        @Suppress("UNUSED_VALUE")
        isOpen = true
    }

    OutlinedTextField(
        enabled = enabled,
        readOnly = true,
        value = textFieldValue,
        onValueChange = { },
        modifier = modifier,
        singleLine = true,
        textStyle = textStyle,
        label = label,
        placeholder = placeholder,
        leadingIcon = leadingIcon,
        trailingIcon = trailingIcon,
        isError = isError,
        visualTransformation = visualTransformation,
        keyboardOptions = KeyboardOptions(imeAction = ImeAction.None),
        keyboardActions = KeyboardActions.Default,
        maxLines = Int.MAX_VALUE,
        interactionSource = interactionSource,
        colors = colors
    )

    @Suppress("UNUSED_VALUE")
    wasFocused = isFocused
}
