package space.gm666q.wasteless.android.ui.navigation

enum class ScreenId {
    CATEGORY,
    EDIT,
    HOME,
    SETTINGS,
}
