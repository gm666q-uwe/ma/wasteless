package space.gm666q.wasteless.android.ui.edit

import androidx.compose.animation.AnimatedVisibility
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.expandVertically
import androidx.compose.animation.shrinkVertically
import androidx.compose.foundation.interaction.MutableInteractionSource
import androidx.compose.foundation.interaction.collectIsFocusedAsState
import androidx.compose.foundation.interaction.collectIsPressedAsState
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.shape.ZeroCornerSize
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.ExpandLess
import androidx.compose.material.icons.twotone.ExpandMore
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.input.VisualTransformation

@OptIn(ExperimentalAnimationApi::class)
@Composable
inline fun <reified E : Enum<E>> EnumField(
    value: E?,
    crossinline onValueChange: (E?) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    noinline nameMapping: @Composable ((E) -> String)? = null,
    noinline label: @Composable (() -> Unit)? = null,
    noinline placeholder: @Composable (() -> Unit)? = null,
    noinline leadingIcon: @Composable (() -> Unit)? = null,
    noinline trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    //keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    //keyboardActions: KeyboardActions = KeyboardActions(),
    //singleLine: Boolean = false,
    //maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    shape: Shape =
        MaterialTheme.shapes.small.copy(bottomEnd = ZeroCornerSize, bottomStart = ZeroCornerSize),
    colors: TextFieldColors = TextFieldDefaults.textFieldColors()
) {
    val isFocused = interactionSource.collectIsFocusedAsState().value
    val isPressed = interactionSource.collectIsPressedAsState().value

    var expanded by remember { mutableStateOf(false) }
    var wasFocused by remember { mutableStateOf(isFocused) }
    val text = if (value == null) "" else nameMapping?.invoke(value) ?: value.name
    var textFieldValueState by remember { mutableStateOf(TextFieldValue(text = text)) }
    val textFieldValue = textFieldValueState.copy(text = text)

    val isFocusedNow = if (wasFocused) false else isFocused

    if ((isFocusedNow || isPressed) && !readOnly) {
        expanded = true
    }

    Box(Modifier.wrapContentSize(Alignment.TopStart)) {
        TextField(
            enabled = enabled,
            readOnly = true,
            value = textFieldValue,
            onValueChange = { },
            modifier = modifier,
            singleLine = true,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon ?: {
                AnimatedVisibility(
                    expanded,
                    enter = expandVertically(),
                    exit = shrinkVertically()
                ) {
                    Icon(Icons.TwoTone.ExpandLess, null)
                }
                AnimatedVisibility(
                    !expanded,
                    enter = expandVertically(),
                    exit = shrinkVertically()
                ) {
                    Icon(Icons.TwoTone.ExpandMore, null)
                }
            },
            isError = isError,
            visualTransformation = visualTransformation,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.None),
            keyboardActions = KeyboardActions.Default,
            maxLines = Int.MAX_VALUE,
            interactionSource = interactionSource,
            shape = shape,
            colors = colors
        )
        DropdownMenu(expanded, { expanded = false }) {
            enumValues<E>().forEach {
                val name = nameMapping?.invoke(it) ?: it.name
                DropdownMenuItem(onClick = {
                    textFieldValueState = TextFieldValue(name)
                    if (value != it) {
                        onValueChange(it)
                    }
                    expanded = false
                }) {
                    Text(name)
                }
            }
        }
    }

    @Suppress("UNUSED_VALUE")
    wasFocused = isFocused
}

@OptIn(ExperimentalAnimationApi::class)
@Composable
inline fun <reified E : Enum<E>> OutlinedEnumField(
    value: E?,
    crossinline onValueChange: (E?) -> Unit,
    modifier: Modifier = Modifier,
    enabled: Boolean = true,
    readOnly: Boolean = false,
    textStyle: TextStyle = LocalTextStyle.current,
    noinline nameMapping: @Composable ((E) -> String)? = null,
    noinline label: @Composable (() -> Unit)? = null,
    noinline placeholder: @Composable (() -> Unit)? = null,
    noinline leadingIcon: @Composable (() -> Unit)? = null,
    noinline trailingIcon: @Composable (() -> Unit)? = null,
    isError: Boolean = false,
    visualTransformation: VisualTransformation = VisualTransformation.None,
    //keyboardOptions: KeyboardOptions = KeyboardOptions.Default,
    //keyboardActions: KeyboardActions = KeyboardActions.Default,
    //singleLine: Boolean = false,
    //maxLines: Int = Int.MAX_VALUE,
    interactionSource: MutableInteractionSource = remember { MutableInteractionSource() },
    colors: TextFieldColors = TextFieldDefaults.outlinedTextFieldColors()
) {
    val isFocused = interactionSource.collectIsFocusedAsState().value
    val isPressed = interactionSource.collectIsPressedAsState().value

    var expanded by remember { mutableStateOf(false) }
    var wasFocused by remember { mutableStateOf(isFocused) }
    val text = if (value == null) "" else nameMapping?.invoke(value) ?: value.name
    var textFieldValueState by remember { mutableStateOf(TextFieldValue(text = text)) }
    val textFieldValue = textFieldValueState.copy(text = text)

    val isFocusedNow = if (wasFocused) false else isFocused

    if ((isFocusedNow || isPressed) && !readOnly) {
        expanded = true
    }

    Box(Modifier.wrapContentSize(Alignment.TopStart)) {
        OutlinedTextField(
            enabled = enabled,
            readOnly = true,
            value = textFieldValue,
            onValueChange = { },
            modifier = modifier,
            singleLine = true,
            textStyle = textStyle,
            label = label,
            placeholder = placeholder,
            leadingIcon = leadingIcon,
            trailingIcon = trailingIcon ?: {
                AnimatedVisibility(
                    expanded,
                    enter = expandVertically(),
                    exit = shrinkVertically()
                ) {
                    Icon(Icons.TwoTone.ExpandLess, null)
                }
                AnimatedVisibility(
                    !expanded,
                    enter = expandVertically(),
                    exit = shrinkVertically()
                ) {
                    Icon(Icons.TwoTone.ExpandMore, null)
                }
            },
            isError = isError,
            visualTransformation = visualTransformation,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.None),
            keyboardActions = KeyboardActions.Default,
            maxLines = Int.MAX_VALUE,
            interactionSource = interactionSource,
            colors = colors
        )
        DropdownMenu(expanded, { expanded = false }) {
            enumValues<E>().forEach {
                val name = nameMapping?.invoke(it) ?: it.name
                DropdownMenuItem(onClick = {
                    textFieldValueState = TextFieldValue(name)
                    if (value != it) {
                        onValueChange(it)
                    }
                    expanded = false
                }) {
                    Text(name)
                }
            }
        }
    }

    @Suppress("UNUSED_VALUE")
    wasFocused = isFocused
}