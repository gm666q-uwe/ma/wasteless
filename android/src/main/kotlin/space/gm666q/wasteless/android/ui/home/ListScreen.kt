package space.gm666q.wasteless.android.ui.home

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.combinedClickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListScope
import androidx.compose.foundation.lazy.items
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import kotlinx.datetime.*
import org.koin.androidx.compose.getViewModel
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.model.Category
import space.gm666q.wasteless.android.ui.WasteLessViewModel
import space.gm666q.wasteless.android.ui.util.pluralsResource
import space.gm666q.wasteless.model.Product
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
@Composable
fun CategoryListScreen(
    category: Category,
    selected: List<Product>,
    onProduct: (Product) -> Unit,
    onSelect: (Product) -> Unit,
    modifier: Modifier = Modifier
) {
    val formatter = remember { DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT) }
    val wasteLessViewModel: WasteLessViewModel = getViewModel()

    val systemTimeZone = remember { TimeZone.currentSystemDefault() }
    val today = Clock.System.now().toLocalDateTime(systemTimeZone).date

    val products = wasteLessViewModel.productsCategory(category).collectAsState()

    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        products.value.groupBy { it.expiration_date }.forEach { (key, products) ->
            listItem(
                key,
                selected,
                onProduct,
                onSelect,
                products,
                expired = today > key,
                formatter = formatter
            )
        }
        item {
            Spacer(modifier)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
@Composable
fun ExpiredListScreen(
    selected: List<Product>,
    onProduct: (Product) -> Unit,
    onSelect: (Product) -> Unit,
    modifier: Modifier = Modifier
) {
    val wasteLessViewModel: WasteLessViewModel = getViewModel()

    val systemTimeZone = remember { TimeZone.currentSystemDefault() }
    val yesterday =
        Clock.System.now().toLocalDateTime(systemTimeZone).date.minus(1, DateTimeUnit.DAY)
    val lastMonth = yesterday.minus(1, DateTimeUnit.MONTH)

    val productCount = wasteLessViewModel.productCount(lastMonth, less = true).collectAsState()
    val products = wasteLessViewModel.productsRange(lastMonth, yesterday).collectAsState()

    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        item {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    pluralsResource(
                        R.plurals.products_expired_over_month,
                        productCount.value.toInt(),
                        productCount.value
                    ),
                    modifier = Modifier.padding(bottom = 4.dp),
                    style = MaterialTheme.typography.body2
                )
            }
        }
        products.value.groupBy { it.expiration_date }.forEach { (key, products) ->
            listItem(key, selected, onProduct, onSelect, products, yesterday, expired = true)
        }
        item {
            Spacer(modifier)
        }
    }
}

@OptIn(ExperimentalFoundationApi::class, ExperimentalMaterialApi::class)
@Composable
fun GoodListScreen(
    selected: List<Product>,
    onProduct: (Product) -> Unit,
    onSelect: (Product) -> Unit,
    modifier: Modifier = Modifier
) {
    val wasteLessViewModel: WasteLessViewModel = getViewModel()

    val systemTimeZone = remember { TimeZone.currentSystemDefault() }
    val today = Clock.System.now().toLocalDateTime(systemTimeZone).date
    val nextMonth = today.plus(1, DateTimeUnit.MONTH)

    val productCount = wasteLessViewModel.productCount(nextMonth).collectAsState()
    val products = wasteLessViewModel.productsRange(today, nextMonth).collectAsState()

    LazyColumn(modifier = Modifier.fillMaxWidth()) {
        products.value.groupBy { it.expiration_date }.forEach { (key, products) ->
            listItem(key, selected, onProduct, onSelect, products, today)
        }
        item {
            Column(
                modifier = Modifier.fillMaxWidth(),
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    pluralsResource(
                        R.plurals.products_expire_in_over_month,
                        productCount.value.toInt(),
                        productCount.value
                    ),
                    modifier = Modifier.padding(top = 4.dp),
                    style = MaterialTheme.typography.body2
                )
            }
        }
        item {
            Spacer(modifier)
        }
    }
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
private fun LazyListScope.listItem(
    date: LocalDate,
    selected: List<Product>,
    onProduct: (Product) -> Unit,
    onSelect: (Product) -> Unit,
    products: List<Product>,
    expired: Boolean = false,
    formatter: DateTimeFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)
) {
    item(date.atStartOfDayIn(TimeZone.UTC).toEpochMilliseconds()) {
        Text(
            stringResource(
                if (expired) R.string.products_expired_on else R.string.products_expire_on,
                date.toJavaLocalDate().format(formatter)
            ),
            color = MaterialTheme.colors.primary,
            modifier = Modifier.padding(start = 8.dp),
            style = MaterialTheme.typography.subtitle2
        )
    }
    items(products) { product ->
        ListItem(modifier = Modifier.combinedClickable(
            onLongClick = if (selected.isEmpty()) {
                { onSelect(product) }
            } else {
                null
            }) { if (selected.isEmpty()) onProduct(product) else onSelect(product) },
            secondaryText = { Text(stringResource(R.string.product_quantity, product.quantity)) },
            trailing = if (selected.isEmpty()) {
                null
            } else {
                {
                    Checkbox(selected.find { it.id == product.id } != null, {})
                }
            }) {
            Text(product.name)
        }
    }
    item {
        Divider()
    }
}

@ExperimentalFoundationApi
@ExperimentalMaterialApi
private fun LazyListScope.listItem(
    date: LocalDate,
    selected: List<Product>,
    onProduct: (Product) -> Unit,
    onSelect: (Product) -> Unit,
    products: List<Product>,
    now: LocalDate,
    expired: Boolean = false,
) {
    item(date.atStartOfDayIn(TimeZone.UTC).toEpochMilliseconds()) {
        val days = if (expired) date.daysUntil(now) else now.daysUntil(date)
        Text(
            if (expired) {
                if (days == 0) {
                    stringResource(R.string.products_expired_yesterday)
                } else {
                    pluralsResource(R.plurals.products_expired_days_ago, days, days)
                }
            } else {
                if (days == 0) stringResource(R.string.products_expire_today) else pluralsResource(
                    R.plurals.products_expire_in_days,
                    days,
                    days
                )

            },
            color = MaterialTheme.colors.primary,
            modifier = Modifier.padding(start = 8.dp),
            style = MaterialTheme.typography.subtitle2
        )
    }
    items(products) { product ->
        ListItem(modifier = Modifier.combinedClickable(
            onLongClick = if (selected.isEmpty()) {
                { onSelect(product) }
            } else {
                null
            }) { if (selected.isEmpty()) onProduct(product) else onSelect(product) },
            secondaryText = { Text(stringResource(R.string.product_quantity, product.quantity)) },
            trailing = if (selected.isEmpty()) {
                null
            } else {
                {
                    Checkbox(selected.find { it.id == product.id } != null, {})
                }
            }) {
            Text(product.name)
        }
    }
    item {
        Divider()
    }
}
