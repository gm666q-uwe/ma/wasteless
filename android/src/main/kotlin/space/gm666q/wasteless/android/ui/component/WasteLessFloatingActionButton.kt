package space.gm666q.wasteless.android.ui.component

import androidx.compose.foundation.shape.CornerSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.twotone.Add
import androidx.compose.material.icons.twotone.Save
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Shape
import androidx.compose.ui.res.stringResource
import com.google.accompanist.insets.navigationBarsWithImePadding
import space.gm666q.wasteless.android.R
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.*

@Composable
fun WasteLessFloatingActionButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    scaffoldMode: WasteLessScaffoldMode = Normal,
    shape: Shape = MaterialTheme.shapes.small.copy(CornerSize(percent = 50))
) {
    when (scaffoldMode) {
        Detail -> {
        }
        DetailSave -> {
            ExtendedFloatingActionButton(
                { Text(stringResource(R.string.fab_save)) },
                onClick,
                modifier = modifier.navigationBarsWithImePadding(),
                icon = { Icon(Icons.TwoTone.Save, null) })
        }
        DetailSelect -> {
        }
        Normal, NormalSelect -> {
            FloatingActionButton(onClick, modifier = modifier, shape = shape) {
                Icon(
                    Icons.TwoTone.Add,
                    stringResource(R.string.cd_add_product)
                )
            }
        }
    }
}
