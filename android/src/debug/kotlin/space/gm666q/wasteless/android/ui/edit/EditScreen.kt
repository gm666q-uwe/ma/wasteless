package space.gm666q.wasteless.android.ui.edit

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun EditContentPreview() {
    ThemedPreview {
        EditContent(null, {}, null, {}, "", {}, "", {}, {})
    }
}

@Preview("Dark colors")
@Composable
private fun EditContentPreviewDark() {
    ThemedPreview(darkTheme = true) {
        EditContent(null, {}, null, {}, "", {}, "", {}, {})
    }
}
