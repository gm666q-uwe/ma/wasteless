package space.gm666q.wasteless.android.ui.component

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun WasteLessBottomAppBarPreview() {
    ThemedPreview {
        WasteLessBottomAppBar({})
    }
}

@Preview("Dark colors")
@Composable
private fun WasteLessBottomAppBarPreviewDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessBottomAppBar({})
    }
}
