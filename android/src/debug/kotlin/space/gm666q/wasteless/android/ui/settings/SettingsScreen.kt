package space.gm666q.wasteless.android.ui.settings

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun SettingsScreenPreview() {
    ThemedPreview {
        SettingsScreen()
    }
}

@Preview("Dark colors")
@Composable
fun PreviewSettingsScreenDark() {
    ThemedPreview(darkTheme = true) {
        SettingsScreen()
    }
}
