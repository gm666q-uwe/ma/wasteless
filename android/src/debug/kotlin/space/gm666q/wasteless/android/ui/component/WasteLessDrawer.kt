package space.gm666q.wasteless.android.ui.component

import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.navigation.View
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun WasteLessDrawerPreview() {
    ThemedPreview {
        val scaffoldState = ScaffoldState(DrawerState(DrawerValue.Open), SnackbarHostState())
        Scaffold(
            scaffoldState = scaffoldState,
            drawerContent = { WasteLessDrawer(currentView = View.GoodList) { _, _ -> } }) {
        }
    }
}

@Preview("Dark colors")
@Composable
private fun WasteLessDrawerPreviewDark() {
    ThemedPreview(darkTheme = true) {
        val scaffoldState = ScaffoldState(DrawerState(DrawerValue.Open), SnackbarHostState())
        Scaffold(
            scaffoldState = scaffoldState,
            drawerContent = { WasteLessDrawer(currentView = View.GoodList) { _, _ -> } }) {
        }
    }
}
