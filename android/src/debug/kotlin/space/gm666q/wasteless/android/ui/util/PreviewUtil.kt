package space.gm666q.wasteless.android.ui.util

import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import space.gm666q.wasteless.android.ui.theme.WasteLessTheme

@Composable
internal fun ThemedPreview(
    darkTheme: Boolean = false,
    content: @Composable () -> Unit
) {
    WasteLessTheme(darkTheme = darkTheme) {
        Surface {
            content()
        }
    }
}
