package space.gm666q.wasteless.android.ui.component

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.*
import space.gm666q.wasteless.android.ui.navigation.View.GoodList
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun WasteLessScaffoldPreview() {
    ThemedPreview {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList
        ) {}
    }
}

@Preview("Dark colors")
@Composable
private fun WasteLessScaffoldPreviewDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList
        ) {}
    }
}

@Preview("Select regular colors")
@Composable
private fun WasteLessScaffoldPreviewSelect() {
    ThemedPreview {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList,
            scaffoldMode = NormalSelect
        ) {}
    }
}

@Preview("Select dark colors")
@Composable
private fun WasteLessScaffoldPreviewSelectDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList,
            scaffoldMode = NormalSelect
        ) {}
    }
}

@Preview("Detail regular colors")
@Composable
private fun WasteLessScaffoldPreviewDetail() {
    ThemedPreview {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList,
            scaffoldMode = Detail
        ) {}
    }
}

@Preview("Detail dark colors")
@Composable
private fun WasteLessScaffoldPreviewDetailDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList,
            scaffoldMode = Detail
        ) {}
    }
}

@Preview("Detail save regular colors")
@Composable
private fun WasteLessScaffoldPreviewDetailSave() {
    ThemedPreview {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList,
            scaffoldMode = DetailSave
        ) {}
    }
}

@Preview("Detail save dark colors")
@Composable
private fun WasteLessScaffoldPreviewDetailSaveDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessScaffold(
            {},
            {},
            {},
            {},
            { _, _ -> },
            {},
            { Text("Title") },
            GoodList,
            scaffoldMode = DetailSave
        ) {}
    }
}
