package space.gm666q.wasteless.android.ui.home

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.model.Category
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun CategoryCardPreview() {
    ThemedPreview {
        CategoryCard(Category.Food, 0)
    }
}

@Preview("Dark colors")
@Composable
private fun CategoryCardPreviewDark() {
    ThemedPreview(darkTheme = true) {
        CategoryCard(Category.Food, 0)
    }
}
