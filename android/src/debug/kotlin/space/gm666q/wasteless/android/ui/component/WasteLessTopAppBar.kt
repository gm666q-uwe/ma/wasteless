package space.gm666q.wasteless.android.ui.component

import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.Detail
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.NormalSelect
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun WasteLessTopAppBarPreview() {
    ThemedPreview {
        WasteLessTopAppBar({}, {}, {}, { Text("Title") })
    }
}

@Preview("Dark colors")
@Composable
private fun WasteLessTopAppBarPreviewDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessTopAppBar({}, {}, {}, { Text("Title") })
    }
}

@Preview("Select regular colors")
@Composable
private fun WasteLessTopAppBarPreviewSelect() {
    ThemedPreview {
        WasteLessTopAppBar({}, {}, {}, { Text("Title") }, scaffoldMode = NormalSelect)
    }
}

@Preview("Select dark colors")
@Composable
private fun WasteLessTopAppBarPreviewSelectDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessTopAppBar({}, {}, {}, { Text("Title") }, scaffoldMode = NormalSelect)
    }
}

@Preview("Detail regular colors")
@Composable
private fun WasteLessTopAppBarPreviewDetail() {
    ThemedPreview {
        WasteLessTopAppBar({}, {}, {}, { Text("Title") }, scaffoldMode = Detail)
    }
}

@Preview("Detail dark colors")
@Composable
private fun WasteLessTopAppBarPreviewDetailDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessTopAppBar({}, {}, {}, { Text("Title") }, scaffoldMode = Detail)
    }
}
