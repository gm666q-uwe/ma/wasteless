package space.gm666q.wasteless.android.ui.component

import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import space.gm666q.wasteless.android.ui.component.WasteLessScaffoldMode.DetailSave
import space.gm666q.wasteless.android.ui.util.ThemedPreview

@Preview("Regular colors")
@Composable
private fun WasteLessFloatingActionButtonPreview() {
    ThemedPreview {
        WasteLessFloatingActionButton({})
    }
}

@Preview("Dark colors")
@Composable
private fun WasteLessFloatingActionButtonPreviewDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessFloatingActionButton({})
    }
}

@Preview("Detail save regular colors")
@Composable
private fun WasteLessFloatingActionButtonPreviewDetailSave() {
    ThemedPreview {
        WasteLessFloatingActionButton({}, scaffoldMode = DetailSave)
    }
}

@Preview("Detail save dark colors")
@Composable
private fun WasteLessFloatingActionButtonPreviewDetailSaveDark() {
    ThemedPreview(darkTheme = true) {
        WasteLessFloatingActionButton({}, scaffoldMode = DetailSave)
    }
}
