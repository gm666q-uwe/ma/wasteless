android {
    buildToolsVersion = libs.versions.android.buildToolsVersion.get()
    compileOptions {
        isCoreLibraryDesugaringEnabled = true
        sourceCompatibility = JavaVersion.VERSION_11
        targetCompatibility = JavaVersion.VERSION_11
    }
    compileSdk = libs.versions.android.compileSdk.orNull?.toIntOrNull()
    defaultConfig {
        minSdk = libs.versions.android.minSdk.orNull?.toIntOrNull()
        targetSdk = libs.versions.android.targetSdk.orNull?.toIntOrNull()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    dependencies {
        coreLibraryDesugaring(libs.android.tools.desugarJDKLibs)
    }
    sourceSets["main"].manifest.srcFile("src/androidMain/AndroidManifest.xml")
}

kotlin {
    android {
        compilations.all {
            kotlinOptions {
                jvmTarget = "11"
            }
        }
    }
    cocoapods {
        frameworkName = "Common"
        homepage = "https://gm666q.space"
        ios.deploymentTarget = "14.0"
        podfile = project.file("../ios/Podfile")
        summary = "Common"
    }
    /*cocoapodsext {
        homepage = "https://gm666q.space"
        framework {
            export(Touchlab.kermit)
            isStatic = false
            transitiveExport = true
        }
        summary = "Common"
    }*/
    ios()
    sourceSets {
        val androidAndroidTest by getting {
            dependencies {
            }
        }
        val androidMain by getting {
            dependencies {
                implementation(libs.bundles.common.android.main)
            }
        }
        val androidTest by getting {
            dependencies {
                implementation(kotlin("test-junit"))
                implementation(libs.bundles.common.android.test)
            }
        }
        val commonMain by getting {
            dependencies {
                api(libs.bundles.common.common.mainApi)

                implementation(libs.bundles.common.common.main)
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-annotations-common"))
                implementation(kotlin("test-common"))
                implementation(libs.bundles.common.common.test)
            }
        }
        val iosMain by getting {
            dependencies {
                implementation(libs.bundles.common.ios.main)
            }
        }
        val iosTest by getting
    }
}

plugins {
    //id("co.touchlab.native.cocoapods")
    id("com.android.library")
    id("com.squareup.sqldelight")
    kotlin("multiplatform")
    kotlin("native.cocoapods")
}

sqldelight {
    database("WasteLessDatabase") {
        packageName = "space.gm666q.wasteless.db"
    }
}
