package space.gm666q.wasteless.di

import co.touchlab.kermit.LogcatLogger
import co.touchlab.kermit.Logger

actual class LoggerFactory {
    actual fun createLogger(): Logger = LogcatLogger()
}
