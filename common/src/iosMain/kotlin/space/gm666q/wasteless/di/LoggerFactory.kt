package space.gm666q.wasteless.di

import co.touchlab.kermit.Logger
import co.touchlab.kermit.NSLogLogger

actual class LoggerFactory {
    actual fun createLogger(): Logger = NSLogLogger()
}
