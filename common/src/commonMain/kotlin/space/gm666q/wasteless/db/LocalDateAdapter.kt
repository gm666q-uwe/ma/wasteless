package space.gm666q.wasteless.db

import com.squareup.sqldelight.ColumnAdapter
import kotlinx.datetime.*

class LocalDateAdapter(private val timeZone: TimeZone = TimeZone.currentSystemDefault()) :
    ColumnAdapter<LocalDate, Long> {

    override fun decode(databaseValue: Long) =
        Instant.fromEpochMilliseconds(databaseValue).toLocalDateTime(timeZone).date

    override fun encode(value: LocalDate) = value.atStartOfDayIn(timeZone).toEpochMilliseconds()
}
