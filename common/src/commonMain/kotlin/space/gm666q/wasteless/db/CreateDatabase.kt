package space.gm666q.wasteless.db

import com.squareup.sqldelight.EnumColumnAdapter
import com.squareup.sqldelight.db.SqlDriver

fun createDatabase(driver: SqlDriver) =
    WasteLessDatabase(driver, Product.Adapter(EnumColumnAdapter(), LocalDateAdapter()))
