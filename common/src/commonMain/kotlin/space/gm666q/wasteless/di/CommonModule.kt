package space.gm666q.wasteless.di

import co.touchlab.kermit.Kermit
import org.koin.dsl.module
import space.gm666q.wasteless.db.DriverFactory
import space.gm666q.wasteless.repository.WasteLessRepository

fun commonModule(driverFactory: DriverFactory, loggerFactory: LoggerFactory) = module {
    single { Kermit(loggerFactory.createLogger()) }
    single { WasteLessRepository(driverFactory.createDriver()) }
}
