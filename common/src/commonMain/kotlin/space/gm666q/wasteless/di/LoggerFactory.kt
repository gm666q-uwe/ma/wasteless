package space.gm666q.wasteless.di

import co.touchlab.kermit.Logger

expect class LoggerFactory {
    fun createLogger(): Logger
}
