package space.gm666q.wasteless.model

import kotlinx.datetime.LocalDate

data class Product(
    val id: Long? = null,
    val category: CategoryId,
    val expiration_date: LocalDate,
    val name: String,
    val quantity: Long
)
