package space.gm666q.wasteless.model

enum class CategoryId {
    FOOD,
    MEDICINE,
    PERSONAL_CARE
}
