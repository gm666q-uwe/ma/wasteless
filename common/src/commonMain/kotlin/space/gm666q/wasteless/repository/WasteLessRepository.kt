package space.gm666q.wasteless.repository

import co.touchlab.kermit.Kermit
import com.squareup.sqldelight.db.SqlDriver
import com.squareup.sqldelight.runtime.coroutines.asFlow
import com.squareup.sqldelight.runtime.coroutines.mapToList
import com.squareup.sqldelight.runtime.coroutines.mapToOne
import com.squareup.sqldelight.runtime.coroutines.mapToOneOrNull
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.MainScope
import kotlinx.datetime.LocalDate
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import space.gm666q.wasteless.db.createDatabase
import space.gm666q.wasteless.model.CategoryId
import space.gm666q.wasteless.model.Product

class WasteLessRepository(driver: SqlDriver) : KoinComponent {
    private val coroutineScope: CoroutineScope = MainScope()
    private val logger: Kermit by inject()
    private val wasteLessDatabase = createDatabase(driver)

    fun countProducts(date: LocalDate) =
        wasteLessDatabase.productQueries.countWhereExpirationDate(date).executeAsOne()

    fun countProductsAsFlow(date: LocalDate, less: Boolean = true) =
        (if (less) wasteLessDatabase.productQueries.countWhereExpirationDateLess(date) else wasteLessDatabase.productQueries.countWhereExpirationDateMore(
            date
        )).asFlow().mapToOne()

    fun deleteProduct(product: Product) {
        wasteLessDatabase.productQueries.deleteWhereId(product.id!!)
    }

    fun fetchProduct(id: Long) =
        wasteLessDatabase.productQueries.selectWhereId(id, ::Product).executeAsOneOrNull()

    fun fetchProductAsFlow(id: Long) =
        wasteLessDatabase.productQueries.selectWhereId(id, ::Product).asFlow().mapToOneOrNull()

    fun fetchProductsAsFlow() =
        wasteLessDatabase.productQueries.selectAll(::Product).asFlow().mapToList()

    fun fetchProductsCategoryAsFlow(category: CategoryId, descending: Boolean = false) =
        (if (descending) wasteLessDatabase.productQueries.selectWhereCategoryDescending(
            category,
            ::Product
        ) else wasteLessDatabase.productQueries.selectWhereCategoryAscending(
            category,
            ::Product
        )).asFlow().mapToList()

    fun fetchProductsRangeAsFlow(low: LocalDate, high: LocalDate, descending: Boolean = false) =
        (if (descending) wasteLessDatabase.productQueries.selectWhereExpirationDateBetweenDescending(
            low,
            high,
            ::Product
        ) else wasteLessDatabase.productQueries.selectWhereExpirationDateBetweenAscending(
            low,
            high,
            ::Product
        )).asFlow().mapToList()

    fun insertProduct(product: Product) {
        wasteLessDatabase.productQueries.insert(
            product.category,
            product.expiration_date,
            product.name,
            product.quantity
        )
    }

    fun updateProduct(product: Product) {
        wasteLessDatabase.productQueries.updateWhereId(
            product.category,
            product.expiration_date,
            product.name,
            product.quantity,
            product.id!!
        )
    }
}
