package space.gm666q.wasteless.build

class Version(var variant: Int, var major: Int, var minor: Int, var patch: Int) {
    val code: Int
        get() = makeCode(variant, major, minor, patch)
    val name: String
        get() = makeName(variant, major, minor, patch)

    companion object {
        val app: Version = Version(0, 0, 4, 1)

        //fun makeCode(major: Int, minor: Int, patch: Int) = (major shl 22) or (minor shl 12) or patch

        fun makeCode(variant: Int, major: Int, minor: Int, patch: Int) =
            (variant shl 29) or (major shl 22) or (minor shl 12) or patch

        //fun makeName(major: Int, minor: Int, patch: Int) = "$major.$minor.$patch"

        fun makeName(variant: Int, major: Int, minor: Int, patch: Int) =
            "$variant.$major.$minor.$patch"
    }
}
