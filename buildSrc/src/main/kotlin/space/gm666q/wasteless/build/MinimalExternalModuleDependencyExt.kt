package space.gm666q.wasteless.build

import org.gradle.api.artifacts.MinimalExternalModuleDependency

val MinimalExternalModuleDependency.dependencyNotation: String
    get() = "$module:$versionConstraint"
