# Waste Less

***This repository uses [Git LFS](https://git-lfs.github.com/)***

## Requirements

Android Studio 2020.3.1 or newer

## Tested device

- Google Pixel 3, Android 11
- Google Pixel 3, Android 12
