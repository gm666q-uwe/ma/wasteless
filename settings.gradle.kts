enableFeaturePreview("VERSION_CATALOGS")

dependencyResolutionManagement {
    /*repositories {
        google()
        mavenCentral()
    }
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)*/
}

include(":android")
include(":common")

rootProject.name = "Waste Less"
