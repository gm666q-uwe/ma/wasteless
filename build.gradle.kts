import space.gm666q.wasteless.build.Version

allprojects {
    repositories {
        google()
        mavenCentral()
    }

    version = Version.app.name
}

buildscript {
    dependencies {
        classpath(libs.android.tools.build.gradle)
        classpath(libs.sqldelight.gradle)
        //classpath(libs.touchlab.kotlinnativecocoapods)
        classpath(kotlin("gradle-plugin", version = libs.versions.kotlin.orNull))
        classpath(kotlin("serialization", version = libs.versions.kotlin.orNull))
    }
    repositories {
        google()
        mavenCentral()
    }
}

tasks {
    register("clean", Delete::class) {
        delete(rootProject.buildDir)
    }

    wrapper {
        distributionType = Wrapper.DistributionType.ALL
        gradleVersion = libs.versions.gradle.orNull
    }
}
